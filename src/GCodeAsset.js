/**
 * Copyright (c) Jazzros Software Development Sp. z o.o. All rights reserved.
 * Licensed under the MIT License. See LICENSE file in the project root for full license information.  
 */

/* eslint-disable guard-for-in */
/* eslint-disable camelcase */
/* eslint-disable no-array-constructor */

import {
  Vec3,
  Color,
  GeomItem,
  AssetItem,
  Lines,
  Mesh,
  loadTextfile,
  Material,
  resourceLoader,
  BooleanParameter,
  NumberParameter,
  ColorParameter,
  FilePathParameter,
} from '@zeainc/zea-engine'

/**
 * Class designed to load and handle `.gcode` files.
 * Which define the geometry and other properties for objects.
 *
 * **Parameters**
 * * **unitsConversion(`NumberParameter`):** Units multiplier. Default is 1;
 * * **pathColor(`ColorParameter`):** Color of the nozzle path lines. Default is red;
 * * **extrudingColor(`ColorParameter`):** Color of the printed material. Default is yellow;
 * * **maxVisibleLayerIndex(`NumberParameter`):** Index of latest visible layer from GCode file. Set to -1 to draw all available layers. Default is -1;
 * * **drawPath(`BooleanParameter`):** Used to control the display of the nozzle travel paths. Default is true;
 * * **FilePath(`FilePathParameter`):** URL to the *.gcode file;
 *
 * **Events**
 * * **loaded:** Triggered once the GCode has been successfully prepared for display.
 *
 * @extends AssetItem
 */
class GCodeAsset extends AssetItem {
  /**
   * Create an GCode asset.
   * @param {string} name - The name of the GCode asset.
   */
  constructor(name) {
    super(name)

    this.loaded = false
    this.maxLayerIndex = -1

    this.addParameter(new NumberParameter('unitsConversion', 1.0))
    this.addParameter(new ColorParameter('pathColor', new Color(1, 0, 0)))
    this.addParameter(new ColorParameter('extrudingColor', new Color(1, 1, 0)))

    const maxVisibleLayerIndexParam = this.addParameter(new NumberParameter('maxVisibleLayerIndex', -1))
    maxVisibleLayerIndexParam.on('valueChanged', () => {
      this.refreshLayers()
    })

    const pathVisibleParam = this.addParameter(new BooleanParameter('drawPath', true))
    pathVisibleParam.on('valueChanged', () => {
      this.refreshLayers()
    })

    const fileParam = this.addParameter(new FilePathParameter('FilePath'))
    fileParam.on('valueChanged', () => {
      this.loaded = false
      this.removeAllChildren()
      this.maxLayerIndex = -1
      const url = fileParam.getUrl()
      this.load(url)
    })
  }

  /**
   * The getMaxLayerIndex method
   */
  getMaxLayerIndex() {
    return this.maxLayerIndex
  }

  /**
   * The __getLayerIndex method.
   * @param {string} name - Layer name.
   * @return {int} - index of layer.
   * @private
   */
  __getLayerIndex(name) {
    const res = /^Layer(\d+)_/g.exec(name)
    if (res && res.length == 2) return parseInt(res[1])
  }

  /**
   * The __getLayerType method.
   * @param {string} name - Layer name.
   * @return {string} - Type of layer ('path' or 'extruding').
   * @private
   */
  __getLayerType(name) {
    const res = name.split('_')
    if (res && res.length == 2) return res[1]
  }

  /**
   * The refreshLayers method
   */
  refreshLayers() {
    if (!this.loaded) return

    const isPathVisible = this.getParameter('drawPath').getValue()

    let maxVisibleLayerIndex = this.getParameter('maxVisibleLayerIndex').getValue()
    if (maxVisibleLayerIndex == -1) maxVisibleLayerIndex = Infinity

    var maxLayerIndex = -1
    for (let index = 0; index < this.getNumChildren(); index++) {
      const geomItem = this.getChild(index)
      const name = geomItem.getName()
      const currentVis = geomItem.getParameter('Visible').getValue()
      const layerType = this.__getLayerType(name)
      const layerIndex = this.__getLayerIndex(name)
      if (layerIndex > maxLayerIndex) maxLayerIndex = layerIndex
      const vis =
        layerIndex <= maxVisibleLayerIndex && ((layerType == 'path' && isPathVisible) || layerType == 'extruding')
      if (currentVis != vis) geomItem.setVisible(vis)
    }
    this.maxLayerIndex = maxLayerIndex
  }

  /**
   * Loads all the geometries and metadata from the GCode file.
   * @param {string} url - The URL of the asset to load
   * @return {Promise} - Returns a promise that resolves once the initial load is complete
   */
  load(url) {
    return new Promise((resolve, reject) => {
      const fileFolder = url.substring(0, url.lastIndexOf('/')) + '/'
      const filename = url.substring(url.lastIndexOf('/') + 1)

      const layers = new Array()

      const parseGCodeData = async (fileData) => {
        // performance.mark("parseGCodeData");

        // array of lines separated by the newline
        const lines = fileData.split('\n')
        const WHITESPACE_RE = /\s+/

        var state = {
          x: 0,
          y: 0,
          z: 0,
          e: 0,
          f: 0,
          extruding: false,
          relative: false,
        }
        let currentLayer = undefined

        function newLayer(p) {
          currentLayer = { vertex: [], pathVertex: [], z: p.z }
          layers.push(currentLayer)
        }

        //Create line segment between p1 and p2
        function addSegment(p1, p2) {
          if (p2.extruding) {
            currentLayer.vertex.push(p1.x, p1.y, p1.z)
            currentLayer.vertex.push(p2.x, p2.y, p2.z)
          } else {
            currentLayer.pathVertex.push(p1.x, p1.y, p1.z)
            currentLayer.pathVertex.push(p2.x, p2.y, p2.z)
          }
        }

        function delta(v1, v2) {
          return state.relative ? v2 : v2 - v1
        }

        function absolute(v1, v2) {
          return state.relative ? v1 + v2 : v2
        }

        for (let i = 0; i < lines.length; i++) {
          let line = lines[i].trim()
          if (line.startsWith(';')) continue
          if (line.includes(';')) line = line.substring(0, line.indexOf(';')).trim() // remove in line comments

          const elements = line.split(WHITESPACE_RE)
          const key = elements.shift()

          var args = {}
          elements.forEach(function (element) {
            if (element[0] !== undefined) {
              var key = element[0].toLowerCase()
              var value = parseFloat(element.substring(1))
              args[key] = value
            }
          })

          switch (key) {
            case 'G0': // G0 - Idle linear movement
            case 'G1': // G1 - Linear Movement
              var currentState = {
                x: args.x !== undefined ? absolute(state.x, args.x) : state.x,
                y: args.y !== undefined ? absolute(state.y, args.y) : state.y,
                z: args.z !== undefined ? absolute(state.z, args.z) : state.z,
                e: args.e !== undefined ? absolute(state.e, args.e) : state.e,
                f: args.f !== undefined ? absolute(state.f, args.f) : state.f,
              }

              // Layer change detection is or made by watching Z, it's made by watching when we extrude at a new Z position
              const d = delta(state.e, currentState.e)
              if (d > 0) {
                currentState.extruding = d > 0
                if (currentLayer == undefined || currentState.z != currentLayer.z) {
                  newLayer(currentState)
                }
              }
              if (currentLayer === undefined) {
                newLayer(currentState)
              }
              addSegment(state, currentState)
              state = currentState
              break
            case 'G2': // G2 - Arc Movement ( clock wise )
            case 'G3': // G3 - Arc Movement ( counter clock wise )
              break
            case 'G20': // G20 - operating mode in inch system
              break
            case 'G21': // G21 - Set Units to Millimeters
              break
            case 'G28': // G28 - Return to reference point
              break
            case 'G90': // G90: Set to Absolute Positioning
              state.relative = false
              break
            case 'G91': // G91: Set to state.relative Positioning
              state.relative = true
              break
            case 'G92': // G92: Set Position
              var currentState = state
              currentState.x = args.x !== undefined ? args.x : currentState.x
              currentState.y = args.y !== undefined ? args.y : currentState.y
              currentState.z = args.z !== undefined ? args.z : currentState.z
              currentState.e = args.e !== undefined ? args.e : currentState.e
              state = currentState
              break
            default: {
              //console.warn('Unhandled line:' + line)
            }
          }
        }
      }

      const buildChildItems = () => {
        // performance.mark("parseGCodeDataDone");
        // performance.mark("buildGCodeTree");

        const pathColor = this.getParameter('pathColor').getValue()
        const extrudingColor = this.getParameter('extrudingColor').getValue()

        const materialPath = new Material('pathMaterial', 'LinesShader')
        materialPath.getParameter('BaseColor').setValue(pathColor)
        materialPath.getParameter('Opacity').setValue(0.5)
        materialPath.getParameter('Overlay').setValue(0.00001)

        // const materialExtruding = new Material('extrudingMaterial', 'LinesShader')
        // materialExtruding.getParameter('BaseColor').setValue( extrudingColor )
        // materialExtruding.getParameter('Opacity').setValue(1)
        // materialExtruding.getParameter('Overlay').setValue(0.00001)

        const materialVolumeExtruding = new Material('extrudingMaterial', 'SimpleSurfaceShader')
        materialVolumeExtruding.getParameter('BaseColor').setValue(extrudingColor)
        materialVolumeExtruding.getParameter('Opacity').setValue(1)

        // const materialVolumeExtruding = new Material('extrudingMaterial', 'StandardSurfaceShader')
        // materialVolumeExtruding.getParameter('BaseColor').setValue( extrudingColor )
        // materialVolumeExtruding.getParameter('Normal').setValue( baseColorTexture )
        // materialVolumeExtruding.getParameter('Roughness').setValue(0.3)
        // materialVolumeExtruding.getParameter('Metallic').setValue(1.0)
        // materialVolumeExtruding.getParameter('Metallic').setValue(0.7)

        for (const layerIndex in layers) {
          // if (layerIndex != 1) continue  // ONLY FOR DEBUG - to build only necessary layer
          const layer = layers[layerIndex]
          buildChildItem(`Layer${layerIndex}_path`, layer.pathVertex, materialPath, false)
          // buildChildItemAuto(`Layer${layerIndex}_extruding`, layer.vertex, materialExtruding)  // to draw lines instead of volume mesh
          buildChildItemVolume(`Layer${layerIndex}_extruding`, layer.vertex, materialVolumeExtruding, 0.1) // 0.05 radius = layer_distance
        }

        this.loaded = true
        this.refreshLayers()

        // Done.
        this.emit('loaded')
        this.getGeometryLibrary().emit('loaded')
        resolve()
      }

      const verticesToVertexIndexes = (vertices) => {
        if (vertices == undefined || vertices.length == 0) return

        const thr = 0.0001
        let numVertices = vertices.length / 3

        let verts = []
        let indicies = []
        let cx1, cy1, cz1
        let cx2, cy2, cz2
        let px, py, pz
        for (let i = 0, ci = 0; i < numVertices; i += 2, ci += 6) {
          cx1 = vertices[ci + 0]
          cy1 = vertices[ci + 1]
          cz1 = vertices[ci + 2]
          cx2 = vertices[ci + 3]
          cy2 = vertices[ci + 4]
          cz2 = vertices[ci + 5]

          const c_diff = Math.abs(cx1 - cx2) > thr || Math.abs(cy1 - cy2) > thr || Math.abs(cz1 - cz2) > thr
          if (!c_diff) {
            console.log(`Segment ${i}-${i + 1} have same coordinates (${cx1}, ${cy1}, ${cz1}) - skipped`)
            const nx = vertices[ci + 6]
            const ny = vertices[ci + 7]
            const nz = vertices[ci + 8]
            const n_diff = Math.abs(nx - cx2) > thr || Math.abs(ny - cy2) > thr || Math.abs(nz - cz2) > thr
            if (n_diff) console.log('WARNING: the next coordinate is different')
            continue
          }

          const p_diff = Math.abs(px - cx1) > thr || Math.abs(py - cy1) > thr || Math.abs(pz - cz1) > thr
          if (px == undefined || p_diff)
            // add point to path
            verts.push(cx1, cy1, cz1)

          verts.push(cx2, cy2, cz2)

          const right_index = verts.length / 3 - 2
          indicies.push(right_index, right_index + 1)

          px = cx2
          py = cy2
          pz = cz2
        }
        return [verts, indicies]
      }

      const buildChildItemAuto = (geomName, vertices, material) => {
        if (vertices.length == 0) return

        const unitsConversion = this.getParameter('unitsConversion').getValue()

        const vis = verticesToVertexIndexes(vertices)
        const verts = vis[0]
        const indicies = vis[1]

        const numVertices = verts.length / 3
        const edges = new Lines(geomName)
        edges.setNumVertices(numVertices)
        edges.__indices = new Uint32Array(indicies)

        const positionsAttr = edges.getVertexAttribute('positions')
        for (let i = 0; i < numVertices; i++) {
          positionsAttr
            .getValueRef(i)
            .set(
              verts[i * 3 + 0] * unitsConversion,
              verts[i * 3 + 1] * unitsConversion,
              verts[i * 3 + 2] * unitsConversion
            )
        }

        const geomItem = new GeomItem(geomName, edges, material)

        this.addChild(geomItem, false)
      }

      const buildChildItem = (geomName, vertices, material, compact) => {
        if (vertices.length == 0) return

        const numVertices = vertices.length / 3
        var indicies
        if (compact) indicies = Array.from({ length: numVertices * 2 }, (_, i) => Math.ceil(i / 2))
        else indicies = [...Array(numVertices).keys()]

        const edges = new Lines(geomName)
        edges.setNumVertices(numVertices)
        edges.__indices = new Uint32Array(indicies)

        const positionsAttr = edges.getVertexAttribute('positions')
        const unitsConversion = this.getParameter('unitsConversion').getValue()

        for (let i = 0; i < numVertices; i++) {
          positionsAttr
            .getValueRef(i)
            .set(
              vertices[i * 3 + 0] * unitsConversion,
              vertices[i * 3 + 1] * unitsConversion,
              vertices[i * 3 + 2] * unitsConversion
            )
        }

        const geomItem = new GeomItem(geomName, edges, material)

        this.addChild(geomItem, false)
      }

      const buildChildItemVolume = (geomName, vertices, material, radius) => {
        if (vertices.length == 0) return

        const vis = verticesToVertexIndexes(vertices)
        const verts = vis[0]
        const indicies = vis[1]

        function toVec3(verts, index) {
          const i = index * 3
          return new Vec3(verts[i], verts[i + 1], verts[i + 2])
        }

        function getAngle(v1, v2) {
          return Math.atan2(v2.y - v1.y, v2.x - v1.x)
        }

        function buildStartingPoint(outVerts, outInds, outTriangles, outQuads, v1, v2, r) {
          const index = outVerts.length / 3
          const angle = getAngle(v1, v2) + Math.PI * 0.5
          const ksr = Math.sin(angle) * r
          const kcr = Math.cos(angle) * r

          outVerts.push(v1.x + kcr, v1.y + ksr, v1.z)
          outVerts.push(v1.x, v1.y, v1.z + r)
          outVerts.push(v1.x - kcr, v1.y - ksr, v1.z)
          outVerts.push(v1.x, v1.y, v1.z - r)
        }

        function buildInternalPoint(outVerts, outInds, outTriangles, outQuads, v1, v2, v3, r) {
          const index = outVerts.length / 3
          const a1 = getAngle(v1, v2)
          const a2 = getAngle(v2, v3)
          const angle = (a1 + a2) * 0.5 + Math.PI * 0.5

          let a // to make circle wider in sharp angles, necessary to keep the thickness of the tube on the edges
          if (
            (Math.abs(v2.x - v1.x) < 0.03 && Math.abs(v2.y - v1.y) < 0.03) ||
            (Math.abs(v3.x - v2.x) < 0.03 && Math.abs(v3.y - v2.y) < 0.03)
          ) {
            a = 1
          } else {
            a = Math.abs(Math.cos(Math.abs(a1 - a2) / 2))
            if (a < 0.1) a = 0.1
          }

          let ksr = (Math.sin(angle) * r) / a
          let kcr = (Math.cos(angle) * r) / a
          if (Math.abs(a1 - a2) > Math.PI) {
            ksr = -ksr
            kcr = -kcr
          }
          outVerts.push(v2.x + kcr, v2.y + ksr, v2.z)
          outVerts.push(v2.x, v2.y, v2.z + r)
          outVerts.push(v2.x - kcr, v2.y - ksr, v2.z)
          outVerts.push(v2.x, v2.y, v2.z - r)

          outQuads.push(index - 4, index - 3, index + 1, index + 0)
          outQuads.push(index - 3, index - 2, index + 2, index + 1)
          outQuads.push(index - 2, index - 1, index + 3, index + 2)
          outQuads.push(index - 1, index - 4, index + 0, index + 3)

          // outQuads.push(index-2, index-1, index+1, index+0)  // for flat lines
        }

        function buildEndingPoint(outVerts, outInds, outTriangles, outQuads, v1, v2, r) {
          const index = outVerts.length / 3
          const angle = getAngle(v1, v2) + Math.PI * 0.5
          const ksr = Math.sin(angle) * r
          const kcr = Math.cos(angle) * r

          outVerts.push(v2.x + kcr, v2.y + ksr, v2.z)
          outVerts.push(v2.x, v2.y, v2.z + r)
          outVerts.push(v2.x - kcr, v2.y - ksr, v2.z)
          outVerts.push(v2.x, v2.y, v2.z - r)

          outQuads.push(index - 4, index - 3, index + 1, index + 0)
          outQuads.push(index - 3, index - 2, index + 2, index + 1)
          outQuads.push(index - 2, index - 1, index + 3, index + 2)
          outQuads.push(index - 1, index - 4, index + 0, index + 3)

          // outQuads.push(index-2, index-1, index+1, index+0)  // for flat lines
        }

        /** Short description: two indexes is 1 segment, to create a pipe wee need to have 2 segments
         * in cycle we need to:
         *     1. Create starting points of segment
         *     2. Fill segment with tube
         *     3. Create ending points of segment
         **/
        const outTriangles = []
        const outQuads = []
        const outVerts = []
        const outInds = []

        // 1. prepare first point
        let i = 0
        let v1 = toVec3(verts, indicies[i])
        let v2 = toVec3(verts, indicies[i + 1])
        buildStartingPoint(outVerts, outInds, outTriangles, outQuads, v1, v2, radius)

        if (indicies.length > 2) {
          let v3, v4

          // 2. prepare all points
          for (i = 2; i < indicies.length - 2; i += 2) {
            // segment 1
            v1 = toVec3(verts, indicies[i])
            v2 = toVec3(verts, indicies[i + 1])
            // segment 2
            v3 = toVec3(verts, indicies[i + 2])
            v4 = toVec3(verts, indicies[i + 3])

            if (indicies[i + 1] == indicies[i + 2]) {
              // path continuous
              buildInternalPoint(outVerts, outInds, outTriangles, outQuads, v1, v2, v4, radius)
            } else {
              // path ending/beginning
              //break  // ONLY FOR DEBUG - to build only first path in layer
              buildEndingPoint(outVerts, outInds, outTriangles, outQuads, v1, v2, radius)

              buildStartingPoint(outVerts, outInds, outTriangles, outQuads, v3, v4, radius)
            }
          }
        }

        // 3. prepare ending point
        v1 = toVec3(verts, indicies[i])
        v2 = toVec3(verts, indicies[i + 1])
        buildEndingPoint(outVerts, outInds, outTriangles, outQuads, v1, v2, radius)

        // build mesh
        const trianglesCount = outTriangles.length / 3
        const quadsCount = outQuads.length / 4
        const verticesCount = outVerts.length / 3

        const mesh = new Mesh(geomName)

        mesh.setNumVertices(verticesCount)

        const positionsAttr = mesh.getVertexAttribute('positions')
        const unitsConversion = this.getParameter('unitsConversion').getValue()

        for (let i = 0; i < verticesCount; i++) {
          positionsAttr
            .getValueRef(i)
            .set(
              outVerts[i * 3 + 0] * unitsConversion,
              outVerts[i * 3 + 1] * unitsConversion,
              outVerts[i * 3 + 2] * unitsConversion
            )
        }

        mesh.setFaceCounts([trianglesCount, quadsCount])
        let faceId = 0
        for (var i2 = 0, triIndex = 0; i2 < trianglesCount; i2++, triIndex += 3) {
          mesh.setFaceVertexIndices(faceId++, [
            outTriangles[triIndex],
            outTriangles[triIndex + 1],
            outTriangles[triIndex + 2],
          ])
        }
        for (var i2 = 0, quaIndex = 0; i2 < quadsCount; i2++, quaIndex += 4) {
          mesh.setFaceVertexIndices(faceId++, [
            outQuads[quaIndex],
            outQuads[quaIndex + 1],
            outQuads[quaIndex + 2],
            outQuads[quaIndex + 3],
          ])
        }

        mesh.computeVertexNormals(Math.PI * 1.1)

        const geomItem = new GeomItem(geomName, mesh, material)

        this.addChild(geomItem, false)
      }

      const loadGCodeData = () => {
        resourceLoader.incrementWorkload(2)
        loadTextfile(
          url,
          (fileData) => {
            resourceLoader.incrementWorkDone(1)
            parseGCodeData(fileData).then(() => {
              buildChildItems()
              resourceLoader.incrementWorkDone(1)
            })
          },
          (error) => {
            this.emit('error', error)
            reject(error)
          }
        )
      }

      loadGCodeData()
    })
  }
}

export { GCodeAsset }
