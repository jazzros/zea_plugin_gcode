# G-CODE
G-CODE is a plug-in for [Zea Engine](https://www.zea.live/en/zea-engine) that adds the possibility to load and display GCode assets by printing layers from first to last or selected. It uses tubes to draw extruded materials and lines to draw paths.

## How to Get Started
### Parameters
- **unitsConversion** - Units multiplier. Default is 1;
- **extrudingColor** - Color of the printed material. Default is yellow;
- **pathColor** - Color of the nozzle path lines. Default is red;
- **maxVisibleLayerIndex** - Index of latest visible layer from GCode file. Set to -1 to draw all available layers. Default is -1;
- **drawPath** - Used to control the display of the nozzle travel paths. Default is true;

### Events
- **loaded** - Triggered once the GCode has been successfully prepared for display

## Usage example
``` js
const { Color } = globalThis.zeaEngine
const { GCodeAsset } = globalThis.gcodeAssetPlugin
const gcodeAsset = new GCodeAsset('insole')  // create a GCode asset object with name "insole"
 
gcodeAsset.getParameter('unitsConversion').setValue(0.1)  // multiplied to 0.1
gcodeAsset.getParameter('pathColor').setValue(new Color(0.93, 0.62, 0.45))  // set light brown color for the nozzle paths
gcodeAsset.getParameter('extrudingColor').setValue(new Color(0.54, 0.31, 0.21))  // set brown color for extruding paths
gcodeAsset.getParameter('maxVisibleLayerIndex').setValue(20)  // draw all layers from 0 to 20
gcodeAsset.getParameter('drawPath').setValue(true)  // show path lines
 
gcodeAsset.load('data/insole.gcode').then(() => {
    console.log(gcodeAsset.getMaxLayerIndex())  // output to the console maximum layer index
    console.log(gcodeAsset.getParameter('drawPath').getValue())  // output to the console value of 'drawPath' parameter
})
```

## Features
| Feature | Status |
| - | - |
| Load GCode | &check; |
| Reload GCode | &check; |
| Optimized volume mesh for extruding paths | &check; |
| Lines for nozzle travel paths | &check; |
| Color can be changed for mesh and lines | &check; |
| Maximum visible layer value can be changed in real-time | &check; |
| Lines for nozzle travel paths can be switched to visible or hidden in real-time | &check; |
| Ability to toggle the display of extruded material with lines or tubes | Coming soon |

## License

Copyright (c) [Jazzros Software Development Sp. z o.o.](https://jazzros.com) All rights reserved.
Licensed under the MIT License. See LICENSE file in the project root for full license information.
